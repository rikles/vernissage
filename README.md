# Vernissage

## Install
```shell
pipx install vernissage
```

## Run
```shell
vernissage image 'input.tif' 'output.jpg'
